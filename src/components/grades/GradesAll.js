import { useContext, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { ApplicationContext } from "../../ApplicationContext";
import { Alert } from "react-bootstrap";
import { GradesTable } from "./GradesTable";
import { GradesService } from "../../services/GradesService";
import { GradesAllFilter } from "./GradesAllFilter";
import { GradesAllToolbar } from "./GradesAllToolbar";
import { Loading } from "../Loading";

export function GradesAll() {
  const { t } = useTranslation();
  const [courses, setCourses] = useState([]);
  const [loading, setLoading] = useState(true);
  const [searchLoading, setSearchLoading] = useState(false);
  const [error, setError] = useState(null);
  const [passedOnly, setPassedOnly] = useState(false);
  const [orderBy, setOrderBy] = useState('semester');
  /**
   * @type {{context:import('@themost/react').ReactDataContext}}
   */
  const { context } = useContext(ApplicationContext);
  const gradesService = new GradesService(context);

  const doSearch = (searchText) => {
    setSearchLoading(true);
    gradesService.getGradeInfoSearch(searchText).then(res => {
      setCourses(res);
      setError(null);
    }).catch(err => {
      setError(err);
      setCourses([]);
    }).finally(() => {
      setSearchLoading(false);
    });
  }

  const orderBySemester = (myCourses) => {
    const semesters = [];
    myCourses.forEach((x) => {
      const semester = x.semester;
      // if semesters contains a semester with the same id then skip
      const found = semesters.find(x => x.id === semester.id);
      if (!found) {
        semesters.push(semester);
      }
    });
    // reverse display order
    return semesters.slice(0).reverse().map((x) => {
      const semesterCourses = myCourses.filter((y) => y.semester.id === x.id);
      return {
        semester: x,
        courses: semesterCourses
      };
    });
  }

  const orderByCourseType = (myCourses) => {
    const courseTypes = [];
    myCourses.forEach((x) => {
      const courseType = x.courseType;
      // if courseTypes contains a courseType with the same id then skip
      const found = courseTypes.find(x => x.id === courseType.id);
      if (!found) {
        courseTypes.push(courseType);
      }
    });
    // reverse display order
    return courseTypes.slice(0).reverse().map((x) => {
      const courseTypeCourses = myCourses.filter((y) => y.courseType.id === x.id);
      return {
        courseType: x,
        courses: courseTypeCourses
      };
    });
  }

  useEffect(() => {
    gradesService.getGradeInfo().then(res => {
      setCourses(res);
      setError(null);
    }).catch(err => {
      setError(err);
      setCourses([]);
    }).finally(() => {
      setLoading(false);
    });
  }, []);

  if (loading) return <Loading />;
  return (
    <>
      <h1 className="mb-4">{t('All Grades')}</h1>
      {loading && (
        <div>Loading...</div>
      )}
      {error && (
        <Alert variant="danger">
          {error.message}
        </Alert>
      )}
      {!error && !loading && (
        <>
          <GradesAllFilter orderBy={orderBy} setOrderBy={setOrderBy} doSearch={doSearch} searchLoading={searchLoading} />
          <GradesAllToolbar passedOnly={passedOnly} setPassedOnly={setPassedOnly} />
          {orderBy === 'semester' && orderBySemester(courses).map((x) => (
            <GradesTable key={x.semester.id} semester={x.semester} courses={x.courses} passedOnly={passedOnly} />
          ))}
          {orderBy === 'courseType' && orderByCourseType(courses).map((x) => (
            <GradesTable key={x.courseType.id} courseType={x.courseType} courses={x.courses} passedOnly={passedOnly} />
          ))}
        </>
      )}
    </>
  );
}
